Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: bagel
Source: http://www.nubakery.org/

Files: *
Copyright: 2009-2015 Toru Shiozaki
           2012-2014 Shane Parker
           2012-2013 Matthew MacLeod
           2012-2013 Matthew Kelley
           2012-2013 Michael Caldwell
           2013      Ryan Reynolds
           2014      Jefferson Bates
License: GPL-3+

Files: src/alglib/*
Copyright: 2005-2013 Sergey Bochkanov
License: GPL-2+

Files: src/alglib/specialfunctions*
Copyright: 1984-2000 by Stephen L. Moshier
License: GPL-2+

Files: src/integral/carsph_gen/mpreal.h
 src/integral/comprys/interpolate/mpreal.h
 src/integral/ecp/testcode/ang_proj/mpreal.h src/integral/ecp/carsph/mpreal.h
 src/integral/ecp/wigner3j_gen/mpreal.h src/integral/rys/interpolate/mpreal.h
Copyright: 2008-2012 Pavel Holoborodko
License: LGPL-2.1+ or BSD-3

Files: debian/*
Copyright: 2016 Michael Banck <mbanck@debian.org>
License: GPL-2+

License: GPL-3+
 This program is free software: you can redistribute it and/or
 modify it under the terms of the GNU General Public License as
 published by the Free Software Foundation, either version 3 of
 the License, or (at your option) any later version.
 .
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 .
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
Comment:
 On Debian systems, the complete text of the GNU General Public 
 License version 3 can be found in `/usr/share/common-licenses/GPL-3'.

License: GPL-2+
 This package is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 3 of the License, or
 (at your option) any later version.
 .
 This package is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 .
 You should have received a copy of the GNU General Public License
 along with this program. If not, see <http://www.gnu.org/licenses/>
 .
Comment: On Debian systems, the complete text of the GNU General Public License
 version 2 can be found in the file  `/usr/share/common-licenses/GPL-2'.

License: LGPL-2.1+
 This library is free software; you can redistribute it and/or
 modify it under the terms of the GNU Lesser General Public
 License as published by the Free Software Foundation; either
 version 2.1 of the License, or (at your option) any later version.
 .
 This library is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 Lesser General Public License for more details.
 .
 You should have received a copy of the GNU Lesser General Public
 License along with this library; if not, write to the Free Software
 Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301 USA
Comment: On Debian systems the complete text of the GNU Lesser General Public
 License 2.1 can be found in the file `/usr/share/common-licenses/LGPL-2.1'.

License: BSD-3
 Redistribution and use in source and binary forms, with or without
 modification, are permitted provided that the following conditions
 are met:
 .
 * Redistributions of source code must retain the above copyright notice,
   this list of conditions and the following disclaimer.
 * Redistributions in binary form must reproduce the above copyright notice,
   this list of conditions and the following disclaimer in the documentation
   and/or other materials provided with the distribution.
 * Neither the name of NextWindow nor the names of its contributors may be
   used to endorse or promote products derived from this software without
   specific prior written permission.
 .
 THIS SOFTWARE IS PROVIDED BY NEXTWINDOW LTD ``AS IS'' AND ANY EXPRESS OR
 IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
 OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN
 NO EVENT SHALL NEXTWINDOW LTD BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
 ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
